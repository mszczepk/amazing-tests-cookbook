<style>
   /* Adjust to your current viewport size */
   div {
      font-size: 3vh;
      width: 50vw;
      height: 100vh;
      margin-left: auto;
      margin-right: auto;
   }
</style>

<div>

   ---
   Amazing Tests Cookbook
   ===
   by mszczepk <img style="transform: translateY(20%); height: 1.5em" src="img/pops.png"></img>

</div>

<div>

   ---
   INB4
   ===
   💀 v0.0.1 (WIP)  
   💀 Rozkminy  
   💀 IMO (z życia wzięte) == dyskusja   
   💀 ~~e2e~~  
   💀 Kotlin

</div>

<div>

   ---
   Why bother?
   ===
   Płacę komuś za zrobienie apki. Skąd wiem, że to wgl działa?</p>
   1) Nie no, raczej działa, nie? 💸 <img style="height: 1.6em" src="img/peepoShy.gif">
   2) Testuję manualnie... <img style="transform: translateY(13%); height:1.2em" src="img/fire.gif">za wolno!<img style="transform: translateY(13%); height:1.2em" src="img/fire.gif">
   3) Poka test... <img style="transform: translateY(13%); height:1.4em" src="img/aha.gif">

</div>

<div>

   ---
   Why bother?
   ===
   Zapominam jak dokładnie działał jeden z ficzerów
   1. Pytam kolegę... _"Ehehe, też już nie pamiętam"_ <img style="transform: translateY(13%); height: 1.5em" src="img/EHEHE.png">
   2. Wchodzę na wiki... nieaktualne, a wręcz nie istnieje
   3. Czytam główny kod... <img style="transform: translateY(13%); height: 1.2em" src="img/fire.gif">ZA WOLNO!!!<img style="transform: translateY(13%); height: 1.2em" src="img/fire.gif">
   4. Poka test... <img style="transform: translateY(13%); height:1.6em" src="img/NotLikeThis.png">

</div>

<div>

   ---
   No więc Why bother?
   ===
   1. Budowanie zaufania (*szansa, że działa <img style="transform: translateY(20%); height: 1.6em" src="img/Pepega.png">)
      * Klientów
      * Nas do nas samych
   2. Budowanie wiedzy

</div>

<div>

   ---
   Kiedy testy są "amazing"?
   ===
   1. Czytelne (dla każdego!)
   2. Szybkie
   3. Łatwe w utrzymaniu  
   4. Otwarte na rozszerzanie
   5. Aż chce się je pisać 

</div>

<div>

   ---
   Let me cook!!!
   ===
   <img style="width: 100%" src="img/letmecook.gif">

</div>

<div>

   ---
   [#1](src/test/kotlin/mszczepk/atc/recipes/1_UseEquals.kt) <br> Asercje `equals`
   ===
   `data class` (kotlin) albo `record` (javsko)  
   ✅ Czytelne  
   ✅ Nie trzeba zmieniać

   Ale jak mam przetestować random uuid albo id-ka z bazy, co? <img style="transform: translateY(13%);height: 1.7em" src="img/sussy.gif"></img>

</div>

<div>

   ---
   [#2](src/test/kotlin/mszczepk/atc/recipes/2_UseApi.kt) <br> Tylko API
   ===
   AKA _"ten sam poziom abstrakcji"_

   <span style="color:yellow;">⚠</span> Ryzyko popełnienia błędu w testach! Najgorsze 💀  
   ✅ Większe bezpieczeństwo  
   ✅ Brak sprzężenia do szczegółów impl (refactoryzacja)  
   
   Ten sam poziom abstrakcji to:  
   💡 Czytelność  
   💡 SRP

   💀 [IRL przykład](src/test/kotlin/mszczepk/atc/recipes/2_UsedInternalsExample.kt)  
   💡 Duplikacja mapowania to nie "duplikacja logiki"

</div>

<div>

   ---
   [#3](src/test/kotlin/mszczepk/atc/recipes/3_DRY.kt) <br> DRY
   ===
   ✅ Krótsze testy  
   ✅ Przydatny wielokrotnie kawałek kodu  
   ✅ Mniejszy próg wejścia  
   ✅ Dokumentacja API w jednym miejscu  
   ✅ Zmiana API nie oznacza zmiany testów... jeśli minor <img style="transform: translateY(13%); height:1.4em" src="img/PepeSmile.png">  

</div>

<div>

   ---
   [#4](src/test/kotlin/mszczepk/atc/recipes/4_DataFactory.kt) <br> Fabryki danych
   ===
   Jak?  
   👉 Happy-path wartości domyślne  
   👉 Konfigurowalne zależności  
   👉 Mapowanie (znamy output)  
   👉 Losowe  

   ✅ Niższy próg wejścia  
   ✅ Łatwe dodawanie przypadków testowych  
   ✅ Wielokrotnego użycia  
   🥶 Duże sprzężenie testów z fabrykami (to zależy)

   👉 [Przykład category path](src/test/kotlin/mszczepk/atc/recipes/4_ShareKnowledgeExample.kt)  
   💡 Testy szerzą wiedzę domenową 
 
   <span style="color:yellow;">⚠</span> Dane testowe są NAJWAŻNIEJSZE!!! Poświęcajmy im najwięcej czasu!

</div>

<div>

   ---
   [#5](src/test/kotlin/mszczepk/atc/recipes/5_WriteClient.kt) <br> Apka klienta
   ===
   ✅ This is what I call TDD!  
   💡 Projektowanie API, nie odkrywanie modelu, ani nie nauka domeny!
   
   </div>

<div>

   ---
   [#6](src/test/kotlin/mszczepk/atc/recipes/6_UseMaps.kt) <br> JSON to mapa
   ===
   ✅ Przypadki nie do uzyskania z POJO  
   ✅ Mniej kodu   

   👉 `String` tylko do bardzo prostych JSONów
   
   👍 Działa dla dowolnego http body

</div>

<div>

   ---
   [#7](src/test/kotlin/mszczepk/atc/recipes/7_Enums.kt)  <br> Enum to test case
   ===
   ✅ Czytelniej:  
   * Widok w IDE (zamiast `test case [0]`, `test case [1]`, itd.)
   * Skupienie na esencji
   * Różnice między przypadkami (dzięki _named params_)

</div>

<div>

   ---
   #8 <br> "proper" i "valid" są useless
   ===
   <img style="height: 1.6em" src="img/ReallyMad.png"></img>  
   `should create proper status from factors`  
   
   <img style="height: 1.6em" src="img/WeSmart.gif"></img>  
   `should pass if conditions are met`  
   `test #1`  

   <img style="height: 1.6em" src="img/okok.gif"></img>  
   `should create status:`  
   `[1] should be STOPPED if now is after end date`

   ✅ Szybsze wejście do tematu

</div>

<div>

   ---
   #9 <br> Kod _pod testy_
   ===

   🟢 DO: Kod, który ma sens, nawet jeśli (na razie) nie jest używany  
      np. Dodanie endpointu pt. "trigger job"  

   ❌ DON'T: Kod, który hackuje nasze własne API  
      np. Asercje oparte na selectach do bazy

</div>

<div>

   ---
   #10 <br> Piramida <img style="transform: translateY(13%); height: 1.6em" src="img/sus2.png"></img>
   ===
   Integracyjne:  
   🔴 Wolne  
   🔴 Trudno wymyślić  
   🔴 Większa szansa, że pominiesz scenariusz  
   🟢 Stabilne  
   🟢 Big picture  
   🟢 Więcej warstw i więcej kontekstu  

   💡 Nie używać Springa? 💖  
   💡 Pisać też unitowe

</div>

<div>

   ---
   #10 <br> Piramida <img style="transform: translateY(13%); height: 1.6em" src="img/sus2.png"></img>
   ===
   Unitowe:  
   🟢 Szybkie  
   🟢 Łatwo wymyślić  
   🟢 Mniejsza szansa, że pominiesz scenariusz  
   🔴 Niestabilne  
   🔴 Small picture  
   🔴 Jedna warstwa oderwana od kontekstu
   
   Przypadek z poziomem abstrakcji (#2) = SRP nie zostało złamane

   💡 Testy unitowe na stabilnych unitach  
   💡 Usuwać testy unita, ale nie usuwać testu funkcjonalności

   In progress...

</div>

<div>

   ---
   Mindset
   ===

   💡 Test jest integralną częścią produktu  
   💡 Jeśli nie napisałem testu, to nie skończyłem  
   💡 Dobre praktyki mają zastosowanie (SOLID)

   Testy nie dowożą żadnej wartości, bez nich szybciej i ficzerek już śmiga, czyż nie? <img style="transform: translateY(10%);height: 1.7em" src="img/sussy.gif"></img>

</div>

<div>

   ---
   TODO
   ===
   👉 Nie Mockito (nulloza, kupa kodu 💩, brak bezpieczeństwa)  
   👉 Testy w tym samym języku (korutyny w Spock 💩 i inne)  
   👉 Nazywanie zmiennych (`delayMillis` vs. `cancelJobAfterMillis`)  
   👉 Walidacja fabryk w testach  
   👉 Wystarczy jeden source set  
   👉 `Array` zamiast `List` (_spread operator_ i _varargs_)  
   👉 Nie ma sensu testować wszystkiego ("trigger job")  
   👉 Nie _Builder Pattern_ 💩 (all deps + _named parameters_ FTW)  


</div>

<div>

   ---
   Final thoughts
   ===
   👍 Trudne... Wymaga dużo kreatywności  
   👍 Nie poddawać się! Ćwiczyć!  
   👍 Wrócić za jakiś czas...  
   👍 Testy pisze się tak samo fajnie, jak główny kod!  
   👍 Warto!  

</div>

<div>

   ---
   Dzinki <img style="transform: translateY(5%); height: 1.5em" src="img/peepoLeaveWave.gif"></img>
   ===
   Sprzężenie zwrotne dla mnie:  
   https://forms.gle/oxUBNK8UgZbU2GeHA

</div>