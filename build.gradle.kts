plugins {
   kotlin("jvm") version "1.9.20"
   kotlin("plugin.spring") version "1.9.20"
   id("org.springframework.boot") version "3.2.0"
   id("io.spring.dependency-management") version "1.1.4"
}

group = "mszczepk.atc"
version = "1.0.0"

repositories {
   mavenCentral()
}

dependencies {
   implementation("org.springframework.boot:spring-boot-starter-actuator")
   implementation("org.springframework.boot:spring-boot-starter-webflux")
   implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
   implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
   implementation("org.jetbrains.kotlin:kotlin-reflect")
   implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

   testImplementation(kotlin("test"))
   testImplementation("org.amshove.kluent:kluent:1.73")
   testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.test {
   useJUnitPlatform()
}

// You do not need two separate source sets!
//val integrationTest = tasks.register<Test>("integrationTest") {
//   description = "Runs integration tests, which are all test classes ending with `IntTest`."
//   group = "verification"
//   useJUnitPlatform()
//   include("*IntTest")
//   shouldRunAfter(tasks.test)
//}
//tasks.check { dependsOn(integrationTest) }

kotlin {
   jvmToolchain(17)
}
