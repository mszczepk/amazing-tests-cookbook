package mszczepk.atc

import java.util.UUID

open class ProductRepository {
   private val productsById: MutableMap<UUID, Product> = mutableMapOf()

   fun findById(id: UUID): Product? =
      productsById[id]

   open fun save(product: Product): Product {
      val current = productsById[product.id]
      val toSave = product.withNextVersion()
      if (current != null && current.version != product.version) {
         throw IllegalArgumentException("Optimistic locking exception!")
      }
      productsById[toSave.id] = toSave
      return toSave
   }

   open fun findAll(): List<Product> = productsById.values.toList().sortedBy { it.id }

   open fun deleteAll() = productsById.clear()
}