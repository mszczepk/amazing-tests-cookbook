package mszczepk.atc

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include.NON_ABSENT
import mszczepk.atc.ProductController.ProductResponse
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.util.Optional
import java.util.UUID

@RestController
class ProductController(
   private val productService: ProductService,
//   private val productService: RefactoredProductService, // 2_UseApi refactored version
) {

   @PostMapping(PRODUCTS_URI)
   fun create(@RequestBody request: CreateProductRequest): ProductResponse =
      productService.create(request)

   @PatchMapping(PRODUCT_URI)
   fun update(@PathVariable id: UUID, @RequestBody request: UpdateProductRequest): ProductResponse =
      productService.update(id, request)

   @GetMapping(PRODUCTS_URI)
   fun getAll(): ProductListResponse =
      productService.getAll()

   @DeleteMapping(PRODUCTS_URI)
   fun deleteAll(): Unit =
      productService.deleteAll()

   data class CreateProductRequest(
      val name: String?,
      val description: String?,
      val price: BigDecimal?,
   )

   @JsonInclude(NON_ABSENT) // Must be null by default to work as "undefined" JSON field. This the PUT approach is better (5_WriteClient).
   data class UpdateProductRequest(
      val name: Optional<String>?,
      val description: Optional<String>?,
      val price: Optional<BigDecimal>?,
      val version: Long,
   )

   data class ProductResponse(
      val id: UUID,
      val name: String?,
      val description: String?,
//      @JsonFormat(shape = STRING) // 6_UseMaps: uncomment to check what type BigDecimal is serialized to by default :P
      val price: BigDecimal?,
      val version: Long,
   )

   data class ProductListResponse(
      val products: List<ProductResponse>,
   )

   companion object {
      const val PRODUCTS_URI = "/products"
      const val PRODUCT_URI = "/products/{id}"
   }

   @PutMapping(PRODUCT_URI)
   fun save(@PathVariable id: UUID, @RequestBody request: ProductResponse): ProductResponse =
      productService.save(id, request)
}

typealias ProductDao = MutableMap<UUID, ProductResponse>