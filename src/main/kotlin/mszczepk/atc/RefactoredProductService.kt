package mszczepk.atc

import mszczepk.atc.ProductController.CreateProductRequest
import mszczepk.atc.ProductController.ProductListResponse
import mszczepk.atc.ProductController.ProductResponse
import mszczepk.atc.ProductController.UpdateProductRequest
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.HttpStatus.PRECONDITION_FAILED
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException
import java.util.UUID
import kotlin.jvm.optionals.getOrNull

@Component
class RefactoredProductService(
   private val productRepository: ProductRepository,
) {

   fun create(request: CreateProductRequest): ProductResponse {
      val toCreate = request.run {
         Product(
            id = UUID.randomUUID(), // SERIO warto tyle kodu tylko po to, żeby wygenerować UUIDa?
            name = name,
            description = description,
            price = price,
            version = 0L,
         )
      }
      val created = productRepository.save(toCreate)
      return created.toResponse()
   }

   fun update(id: UUID, request: UpdateProductRequest): ProductResponse {
      var toUpdate = productRepository.findById(id) ?: throw ResponseStatusException(NOT_FOUND)
      if (toUpdate.version != request.version) {
         throw ResponseStatusException(PRECONDITION_FAILED)
      }
      request.run {
         if (name != null) {
            toUpdate = toUpdate.copy(name = name.getOrNull())
         }
         if (description != null) {
            toUpdate = toUpdate.copy(description = description.getOrNull())
         }
         if (price != null) {
            toUpdate = toUpdate.copy(price = price.getOrNull())
         }
      }
      val updated = productRepository.save(toUpdate)
      return updated.toResponse()
   }

   fun getAll(): ProductListResponse =
      ProductListResponse(
         products = productRepository.findAll().map { it.toResponse() }
      )

   fun deleteAll() {
      productRepository.deleteAll()
   }

   private fun Product.toResponse() =
      ProductResponse(
         id = id,
         name = name,
         description = description,
         price = price,
         version = version
      )

   private fun ProductResponse.toProduct() =
      Product(
         id = id,
         name = name,
         description = description,
         price = price,
         version = version
      )

   // This is for refactored version after TDD applied!
   fun save(id: UUID, request: ProductResponse): ProductResponse {
      val toSave = request.toProduct()
      return try {
         productRepository.save(toSave).toResponse()
      } catch (e: IllegalArgumentException) {
         throw ResponseStatusException(PRECONDITION_FAILED)
      }
   }
}