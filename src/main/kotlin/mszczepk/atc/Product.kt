package mszczepk.atc

import java.math.BigDecimal
import java.util.UUID

data class Product(
    val id: UUID,
    val name: String?,
    val description: String?,
    val price: BigDecimal?,
    val version: Long,
) {
   init {
      // This is the whole domain logic of this service :)
      name?.let { ensure(it.isNotBlank()) }
      description?.let { ensure(it.isNotBlank()) }
      price?.let { ensure(it >= BigDecimal("0.00")) }
      ensure(version >= 0L)
   }

   fun withNextVersion(): Product =
      copy(version = version + 1L)
}