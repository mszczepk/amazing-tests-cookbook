package mszczepk.atc

import mszczepk.atc.ProductController.CreateProductRequest
import mszczepk.atc.ProductController.ProductListResponse
import mszczepk.atc.ProductController.ProductResponse
import mszczepk.atc.ProductController.UpdateProductRequest
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.HttpStatus.PRECONDITION_FAILED
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException
import java.math.BigDecimal
import java.util.UUID
import kotlin.jvm.optionals.getOrNull

@Component
class ProductService(
   private val productDao: ProductDao,
) {

   fun create(request: CreateProductRequest): ProductResponse {
      val toCreate = request.run {
         ProductResponse(
            id = UUID.randomUUID(), // Is it REALLY worth to maintain so much code just to generate random UUID on the server side? xD
            name = name,
            description = description?.uppercase(), // USE EQUALS BUG!
//            description = description,
            price = price,
            version = 1L,
         )
      }
      validate(toCreate)
      productDao[toCreate.id] = toCreate
      return toCreate
   }

   fun update(id: UUID, request: UpdateProductRequest): ProductResponse {
      var toUpdate = productDao[id] ?: throw ResponseStatusException(NOT_FOUND)
      if (toUpdate.version != request.version) {
         throw ResponseStatusException(PRECONDITION_FAILED)
      }
      request.run {
         if (name != null) {
            toUpdate = toUpdate.copy(name = name.getOrNull())
         }
         if (description != null) {
            toUpdate = toUpdate.copy(description = description.getOrNull())
         }
         if (price != null) {
            toUpdate = toUpdate.copy(price = price.getOrNull())
         }
         toUpdate = toUpdate.copy(version = version + 1L)
      }
      validate(toUpdate)
      productDao[toUpdate.id] = toUpdate
      return toUpdate
   }

   fun getAll(): ProductListResponse =
      ProductListResponse(
         products = productDao
            .values
            .toList()
            .sortedBy { it.id } // Makes tests predictable and brings no harm to the main sourcecode :) Probably needed in the future anyway.
      )

   fun deleteAll() {
      productDao.clear()
   }

   // This is the whole domain logic of this service :)
   private fun validate(product: ProductResponse): Unit = product.run {
      name?.let { ensure(it.isNotBlank()) }
      description?.let { ensure(it.isNotBlank()) }
      price?.let { ensure(it >= BigDecimal("0.00")) }
      ensure(version >= 0L)
   }

   // This is for refactored version after TDD applied!
   fun save(id: UUID, request: ProductResponse): ProductResponse {
      var toUpdate = productDao[id]
      if (toUpdate != null && toUpdate.version != request.version) {
         throw ResponseStatusException(PRECONDITION_FAILED)
      }
      toUpdate = request.run { copy(version = version + 1L) }
      validate(toUpdate)
      productDao[id] = toUpdate
      return toUpdate
   }
}