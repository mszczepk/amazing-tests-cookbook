package mszczepk.atc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class Config {

   @Bean
   fun productDao(): ProductDao = mutableMapOf()

   @Bean
   fun productRepository(): ProductRepository = ProductRepository()

   @Bean
   fun mordeczko(): String = "elo"
}

object Profiles {
   const val DEV = "dev"
   const val PROD = "prd"
   const val INTEGRATION = "integration"
}

fun main(args: Array<String>) {
   runApplication<Config>(*args)
}

