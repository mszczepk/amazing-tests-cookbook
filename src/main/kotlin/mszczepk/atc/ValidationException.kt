package mszczepk.atc

import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.ProblemDetail
import org.springframework.web.ErrorResponseException
import org.springframework.web.server.ResponseStatusException

class ValidationException : ResponseStatusException(
    BAD_REQUEST,
//    ProblemDetail.forStatusAndDetail(BAD_REQUEST, "Invalid input"),
//    null,
)

fun ensure(shouldBeTrue: Boolean) {
    if (!shouldBeTrue) {
        throw ValidationException()
    }
}