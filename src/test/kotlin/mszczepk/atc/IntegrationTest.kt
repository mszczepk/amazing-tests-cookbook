package mszczepk.atc

import mszczepk.atc.ProductController.ProductResponse
import mszczepk.atc.Profiles.INTEGRATION
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.FluxExchangeResult
import org.springframework.test.web.reactive.server.WebTestClient
import java.util.UUID
import java.util.UUID.randomUUID

@ActiveProfiles(INTEGRATION)
@AutoConfigureWebTestClient(timeout = "PT5M")
@SpringBootTest(
   classes = [IntTestConfig::class],
   webEnvironment = RANDOM_PORT,
)
abstract class IntegrationTest {

   @Autowired
   lateinit var client: WebTestClient

   companion object {

      inline val <reified T> FluxExchangeResult<T>.body: T?
         get() = responseBody.blockFirst()

      fun ProductResponse.withIgnoredId() =
         copy(id = IGNORED_ID)

      val IGNORED_ID: UUID = randomUUID()
   }
}

@TestConfiguration
class IntTestConfig {

   @Bean
   fun mordeczko(): String = "mordeczko"
}