package mszczepk.atc.recipes

import mszczepk.atc.IntegrationTest
import mszczepk.atc.ProductController.Companion.PRODUCTS_URI
import mszczepk.atc.ProductController.CreateProductRequest
import mszczepk.atc.ProductController.ProductResponse
import org.amshove.kluent.shouldBeEqualTo
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.HttpStatus.OK
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import java.math.BigDecimal
import kotlin.test.BeforeTest
import kotlin.test.Test

class `1_UseEquals` : IntegrationTest() {

   @BeforeTest
   fun reset() {
      client.delete()
         .uri(PRODUCTS_URI)
         .exchange()
         .expectStatus().isOk
   }

   // Shows that field by field assertions are a pain.
   @Test
   fun `should create product`() {
      // given
      val create = CreateProductRequest(
         name = "name",
         description = "description",
         price = BigDecimal("100.00"),
      )

      // when
      val created = client.post()
         .uri(PRODUCTS_URI)
         .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
         .bodyValue(create)
         .exchange()
         .returnResult(ProductResponse::class.java)

      // then
      created.status shouldBeEqualTo OK
      created.body?.run {
         name shouldBeEqualTo "name"
         // Test passes even though there is a bug in "description" field!
         price shouldBeEqualTo BigDecimal("100.00")
      }
   }

   // Shows that test with equals assertions are nice, but it still has shortcomings.
   @Test
   fun `should create product (USED EQUALS)`() {
      val create = CreateProductRequest(
         name = "name",
         description = "description",
         price = BigDecimal("100.00"),
      )
      val expected = ProductResponse(
         id = IGNORED_ID,
         name = "name",
         description = "description",
         price = BigDecimal("100.00"),
         version = 1L,
      )

      // when
      val created = client.post()
         .uri(PRODUCTS_URI)
         .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
         .bodyValue(create)
         .exchange()
         .returnResult(ProductResponse::class.java)

      // then
      created.status shouldBeEqualTo OK
      // Bug spotted, but you have to exclude unpredictable field...
      // You can inject something like "TestUuidGenerator", but it's a pain!!!
      // We'll improve this later!
      created.body?.copy(id = IGNORED_ID) shouldBeEqualTo expected
   }

}