package mszczepk.atc.recipes

import mszczepk.atc.IntegrationTest
import mszczepk.atc.IntegrationTest.Companion
import mszczepk.atc.IntegrationTest.Companion.IGNORED_ID
import mszczepk.atc.ProductController.Companion.PRODUCT_URI
import mszczepk.atc.ProductController.CreateProductRequest
import mszczepk.atc.ProductController.ProductResponse
import mszczepk.atc.ProductController.UpdateProductRequest
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldNotBeEqualTo
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.OK
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.test.web.reactive.server.FluxExchangeResult
import org.springframework.test.web.reactive.server.WebTestClient
import java.math.BigDecimal
import java.util.Optional
import java.util.UUID
import java.util.UUID.randomUUID
import kotlin.jvm.optionals.getOrNull
import kotlin.test.BeforeTest
import kotlin.test.Test

class `6_UseMaps` : IntegrationTest(), JsonProductClient, ProductClient {

   @BeforeTest
   fun reset() {
      deleteProducts()
   }

   // This is the slightly modified version of the previous `should save product` test.
   // Shows that you can't test some scenarios with POJO.
   @Test
   fun `should set field to null`() {
      // given
      val toCreate = CreateProductRequest(
         name = "name",
         description = "description",
         price = BigDecimal("1.00"),
      )
      val created = ProductResponse(
         id = IGNORED_ID,
         name = "name",
         description = "description",
         price = BigDecimal("1.00"),
         version = 1L,
      )
      val toUpdate = UpdateProductRequest(
         name = null, // As we saw in 4_SetupData `should update product`, this should keep the previous value.
         description = Optional.empty(), // This should set to null... I think
         price = null,
         version = 1L,
      )
      val updated = ProductResponse(
         id = IGNORED_ID,
         name = "name",
         description = null,
         price = BigDecimal("1.00"),
         version = 2L,
      )

      // when
      val id = postProduct(toCreate).let {
         val body = it.body // Can be read only once. Uh, so many things to do...
         val id = body!!.id

         // then
         it.status shouldBeEqualTo OK
         body.withIgnoredId() shouldBeEqualTo created
         id
      }

      // when
      patchProduct(id, toUpdate).let {

         // then
         it.status shouldBeEqualTo OK
         it.body shouldBeEqualTo updated.copy(id = id)
         // TEST FAILS because "description" wasn't set to null as expected.
         // How can I set up this test case? :(
         // I don't want to create new DTO for each possible combination :(
      }
   }

   // This is the above test but written in "as client" style.
   // Its setup looks a little better, but the factories are a little pain...
   @Test
   fun `should set field to null (AS CLIENT)`() {
      // given
      val created = productResponse()
      val toCreate = created.toCreateRequest()
      val toUpdate = created.toUpdateRequest(
         name = null,
         description = Optional.empty(),
         price = null,
      )
      val updated = toUpdate.toProductResponse(previous = created)
      // Setup was so long that I moved it to my factories, but there are so many mapping.
      // IRL POJOs hardly differs between each other (CRUD).

      // when
      val id = postProduct(toCreate).let {
         val body = it.body
         val id = body!!.id

         // then
         it.status shouldBeEqualTo OK
         body.withIgnoredId() shouldBeEqualTo created
         id
      }

      // when
      patchProduct(id, toUpdate).let {

         // then
         it.status shouldBeEqualTo OK
         it.body shouldBeEqualTo updated.copy(id = id)
      }
   }

   // Shows can't test some scenarios with POJO. No flexibility as well.
   @Test
   fun `should fail if price is negative`() {
      // given
      val toCreate = product(
         price = BigDecimal("-1.00"),
      )

      // expect
      saveProduct(toCreate).let {
         it.status shouldBeEqualTo BAD_REQUEST
         it.body shouldNotBeEqualTo null
      }
      // I don't event wanted to check the actual response body (it would be nice though), but
      // the test fails because cannot parse the body :/
      // I don't want to create another utility method, just for this case :/
   }

   // Shows that you deal with real JSONs under the hood, but the framework doesn't tell you the truth.
   @Test
   fun `should preserve type of price`() {
      // given
      val toCreate = product(
         price = BigDecimal("1.23")
      )
      val created = toCreate.withNextVersion()

      // expect
      saveProduct(toCreate).let {
         it.status shouldBeEqualTo OK
         it.body shouldBeEqualTo created // Yeah, no way it happens right? Test passes...
      }
   }

   // Shows that using maps is nice and enables to test more scenarios.
   @Test
   fun `should set field to null (USING JSON)`() {
      // given
      val toCreate = productJson(
         name = "Name 123",
         description = "description",
         price = 1.51,
      )
      val created = toCreate.withNextVersion()
      val toUpdate = created // UpdateProductRequest and ProductResponse are so similar, that you can use them interchangeably in test and it passes!
         .withUpdated(description = null)
         .without("name", "price")
      val updated = created
         .withNextVersion()
         .withUpdated(description = null)

      // and
      saveProduct(toCreate).status shouldBeEqualTo OK

      // when
      patchProduct(toUpdate).let {

         // then
         it.status shouldBeEqualTo OK
         it.body shouldBeEqualTo updated
      }
   }

   // Shows the refactored scenario from above where you can test the scenario with maps.
   @Test
   fun `should fail if price is negative (USING JSON)`() {
      // given
      val toCreate = productJson(
         price = "-1.00", // I can skip unimportant fields. I know the defaults represents a valid case :)
      )

      // expect
      saveProduct(toCreate).let {
         it.status shouldBeEqualTo BAD_REQUEST
         it.body shouldBeEqualTo mapOf( // I can even assert ad-hoc structure if it's used only in this test case :)
            "type" to "about:blank",
            "title" to "Bad Request",
            "status" to 400,
            "instance" to "/products/${toCreate.id}"
         )
      }
   }

   // Shows that using maps makes your test more trustworthy as it's closer to the IRL and
   // you get to know the framework better.
   @Test
   fun `should preserve type of price (USING JSON)`() {
      // given
      val toCreate = productJson(
         price = BigDecimal("1.21"),
//         price = "1.21",
      )
      val created = toCreate
         .withNextVersion()
//         .withUpdated(price = 1.21)

      // expect
      saveProduct(toCreate).let {
         it.status shouldBeEqualTo OK
         it.body shouldBeEqualTo created// FAILS?? How come?
      }
   }
}

typealias Json = Map<String, Any?>

interface JsonProductClient {

   val client: WebTestClient

   fun saveProduct(resource: Json): FluxExchangeResult<Json> =
      client
         .put()
         .uri(PRODUCT_URI, resource.id)
         .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
         .bodyValue(resource)
         .exchange()
         .returnResult(Map::class.java) as FluxExchangeResult<Json>

   // JSON version
   fun patchProduct(resource: Json): FluxExchangeResult<Json> =
      client
         .patch()
         .uri(PRODUCT_URI, resource.id)
         .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
         .bodyValue(resource)
         .exchange()
         .returnResult(Map::class.java) as FluxExchangeResult<Json>

   fun productResponse(
      id: UUID = IGNORED_ID,
      name: String = "name",
      description: String = "description",
      price: BigDecimal = BigDecimal("1.00"),
      version: Long = 1L,
   ) = ProductResponse(
      id = id,
      name = name,
      description = description,
      price = price,
      version = version
   )

   fun ProductResponse.toCreateRequest() =
      CreateProductRequest(
         name = name,
         description = description,
         price = price
      )

   fun ProductResponse.toUpdateRequest(
      name: Optional<String>? = Optional.ofNullable(this.name),
      description: Optional<String>? = Optional.ofNullable(this.description),
      price: Optional<BigDecimal>? = Optional.ofNullable(this.price),
      version: Long = this.version
   ) =
      UpdateProductRequest(
         name = null,
         description = description,
         price = price,
         version = version,
      )

   fun UpdateProductRequest.toProductResponse(previous: ProductResponse) =
      ProductResponse(
         id = previous.id,
         name = name?.getOrNull() ?: previous.name,
         description = description?.getOrNull() ?: previous.name,
         price = price?.getOrNull() ?: previous.price,
         version = previous.version + 1L,
      )

   // Provide reasonable defaults :)
   fun productJson(
      id: UUID = randomUUID(),
      name: String? = randomIntsString(),
      description: String? = randomIntsString(),
      // JSON does not have BigDecimal type. Even if you're not aware of it,
      // you end up with either String or Number.
      // What's more you can see that Spring (WebClient/server) actually
      // accepts all String, Double and BigDecimal types.
      price: Any? = "${randomIntsString()}.00",
      version: Int = 0,
   ): Json =
      mapOf(
         "id" to id.toString(),
         "name" to name,
         "description" to description,
         "price" to price,
         "version" to version
      )

   // Works for any JSON builder as long as you follow the convention :)
   fun Json.withNextVersion() =
      this + ("version" to version + 1)

   fun Json.withRandomUpdate() =
      this + mapOf(
         "name" to randomIntsString(),
         "description" to randomIntsString(),
         "price" to BigDecimal("${randomIntsString()}.00"),
      )

   val Json.id: String get() =
      this["id"] as String

   val Json.version: Int get() =
      this["version"] as Int

   fun Json.without(vararg keys: String): Json =
      keys.fold(this) { json, key -> json - key }

   fun Json.withUpdated(
      name: String? = this["name"] as? String,
      description: String? = this["description"] as? String,
      price: Any? = this["price"],
   ): Json =
      this + mapOf(
         "name" to name,
         "description" to description,
         "price" to price,
      )
}