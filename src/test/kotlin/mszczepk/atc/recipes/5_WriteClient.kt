package mszczepk.atc.recipes

import mszczepk.atc.IntegrationTest
import mszczepk.atc.ProductController.Companion.PRODUCTS_URI
import mszczepk.atc.ProductController.Companion.PRODUCT_URI
import org.amshove.kluent.shouldBeEqualTo
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.HttpStatus.OK
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.test.web.reactive.server.FluxExchangeResult
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.returnResult
import java.math.BigDecimal
import java.util.UUID
import java.util.UUID.randomUUID
import kotlin.test.BeforeTest
import kotlin.test.Test
import mszczepk.atc.ProductController.ProductResponse as ProductResource

class `5_WriteClient` : IntegrationTest(), ProductClient {

   @BeforeTest
   fun reset() {
      deleteProducts()
   }

   // Shows represents similar use case as the previous `should update product`, but
   // the application was refactored to replace POST + PATCH with PUT.
   //
   // Writing annoying things in tests showed us that the src/main code could be simplified.
   //
   // Also, this is as readable as can be IMO, although there is still room for improvement.
   @Test
   fun `should save product (REFACTORED FOR CLIENT)`() {
      // given
      val toCreate = product(
         id = randomUUID(), // TBH I don't really care about these values. We use `randomUpdate` below, anyway...
         name = "name",
         description = "description",
         price = BigDecimal("100.00"),
         version = 0L,
      )
//      val toCreate = product() // Uncomment if you realize that you don't care about some values when using (2) "happy-path defaults" from previous `4_DataFactory`
      val created = toCreate.withNextVersion()
      val toUpdate = created.withRandomUpdate() // See how much it has simplified compared to the previous version with PATCH
      val updated = toUpdate.withNextVersion()

      // when
      saveProduct(toCreate).let {// Finally, no more annoying code here :)

         // then
         it.status shouldBeEqualTo OK
         it.body shouldBeEqualTo created
      }

      // when
      saveProduct(toUpdate).let {

         // then
         it.status shouldBeEqualTo OK
         it.body shouldBeEqualTo updated
      }
   }
}

// Simply added PUT API after refactor.
interface ProductClient : ProductApi {

   fun saveProduct(resource: ProductResource): FluxExchangeResult<ProductResource> =
      client
         .put()
         .uri(PRODUCT_URI, resource.id)
         .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
         .bodyValue(resource)
         .exchange()
         .returnResult(ProductResource::class.java)

   fun product(
      id: UUID = randomUUID(),
      name: String? = "name",
      description: String? = "description",
      price: BigDecimal? = BigDecimal("100.00"),
      version: Long = 0L,
   ): ProductResource =
      ProductResource(
         id = id,
         name = name,
         description = description,
         price = price,
         version = version
      )

   fun ProductResource.withNextVersion() =
      copy(version = version + 1L)

   fun ProductResource.withRandomUpdate() =
      copy(
         name = randomIntsString(),
         description = randomIntsString(),
         price = BigDecimal("${randomIntsString()}.00"),
         version = version,
      )

   // This is effectively the same as the previous API with optionals!
   // BUT much easier and less code is needed :)
   fun ProductResource.withUpdated(
      name: String? = this.name,
      description: String? = this.description,
      price: BigDecimal? = this.price,
   ): ProductResource =
      copy(
         name = name,
         description = description,
         price = price,
      )
}