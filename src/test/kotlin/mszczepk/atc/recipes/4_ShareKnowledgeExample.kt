package mszczepk.atc.recipes

import mszczepk.atc.recipes.CategoryPathTest.ProductWithCategoryPath.CategoryPath
import java.util.UUID
import java.util.UUID.randomUUID
import kotlin.test.Test

// Shows that good enough model does not always solve all problems
// as far as information about the system is concerned.
// Therefore, consider putting IRL examples in your test data factories
// to impart some domain knowledge to the reader.

class CategoryPathTest {

   data class ProductWithCategoryPathString(
      val id: UUID,
      val categoryPathString: String, // Is it a good model?
   )

   @Test
   fun `should create product with category path`() {
      // given
      // Now I want to instantiate product with category in your test:
      val p1 = ProductWithCategoryPathString(
         id = randomUUID(),
         categoryPathString = "Zalando>Męskie>Obuwie>Obuwie sportowe", // uhh... was it ">" or "/" I don't remember :( Gotta check in src/main (PAIN)
      )

      // ...
   }

   data class ProductWithCategoryPath(
      val id: UUID,
      val channel: String,
      val categoryPath: CategoryPath, // Still has problems (see below)
   ) {

      data class CategoryPath(
         val categories: List<String>,
      ) {
         override fun toString() = categories.joinToString("/")
      }
   }

   @Test
   fun `should create product with category path (BETTER MODEL)`() {
      // given
      val p1 = ProductWithCategoryPath(
         id = randomUUID(),
         channel = "Zalando_PL",
         categoryPath = CategoryPath(
            categories = listOf( // Now I don't need to worry about separator - better!
               "Zalando", // But should it start with root category "Zalando" or not? There is `channel` property as well... Not sure, gotta check in src/main again (PAIN)
               "Męskie",
               "Obuwie",
               "Obuwie sportowe"
            )
         )
      )

      // ...
   }
}