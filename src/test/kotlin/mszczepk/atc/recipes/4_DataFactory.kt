package mszczepk.atc.recipes

import mszczepk.atc.IntegrationTest
import mszczepk.atc.IntegrationTest.Companion.IGNORED_ID
import mszczepk.atc.ProductController.CreateProductRequest
import mszczepk.atc.ProductController.ProductResponse
import mszczepk.atc.ProductController.UpdateProductRequest
import org.amshove.kluent.shouldBeEqualTo
import org.springframework.http.HttpStatus.OK
import java.math.BigDecimal
import java.util.Optional
import java.util.UUID
import kotlin.jvm.optionals.getOrNull
import kotlin.random.Random
import kotlin.test.BeforeTest
import kotlin.test.Test

class `4_DataFactory` : IntegrationTest(),
   ProductApi,
   CoupledProductBuilder,
   ChainedProductBuilder {

   @BeforeTest
   fun reset() {
      deleteProducts()
   }

   // Shows how long the IRL test can be because of test data in the "given" section.
   // Also, there is a lot of copy-pasting which is a pain,
   // especially if you happen to need to change something in all tests that copy-pasted.
   @Test
   fun `should update product`() {
      // given
      val toCreate = CreateProductRequest(
         name = "name",
         description = "description",
         price = BigDecimal("1.00"),
      )
      val created = ProductResponse(
         id = IGNORED_ID, // Ignore this ugly for now, we'll fix it in the next `5_WriteClient`
         name = "name",
         description = "description",
         price = BigDecimal("1.00"),
         version = 1L,
      )
      val toUpdate = UpdateProductRequest(
         name = Optional.of("new name"),
         description = null, // Will it unset description or keep unchanged? Not sure...
         price = null, // The test will tell the truth, but the API seems a little confusing...
         version = 1L,
      )
      val updated = ProductResponse(
         id = IGNORED_ID, // This is known by test method at some point. We'll set it later, but it's a pain...
         name = "new name",
         description = "description", // OK, unchanged! So surely Optional.empty() sets fields to null, right? We'll see later...
         price = BigDecimal("1.00"),
         version = 2L,
      )

      // when
      val id = postProduct(toCreate).let {
         val body = it.body // Can be read only once. Uh, so many things to do...
         val id = body!!.id

         // then
         it.status shouldBeEqualTo OK
         body.withIgnoredId() shouldBeEqualTo created
         id
      }

      // when
      patchProduct(id, toUpdate).let {

         // then
         it.status shouldBeEqualTo OK
         it.body shouldBeEqualTo updated.copy(id = id)
      }
      // This test is so long... Doesn't fit in my screen.
   }

   // This is the test above but refactored with the 1st approach to coupled factories.
   // Better but stil with problems. I used this approach several times at work, and it's really mid TBH.
   //
   // Used techniques:
   // (1) Happy-path defaults
   // (2) All dependencies configurable
   //
   // The idea is to build other test cases on top of the happy-path.
   // All deps enable you to get rid of Builder Pattern with named parameters.
   @Test
   fun `should update product (WITH COUPLED FACTORIES)`() {
      // given
      val toCreate = createProductRequest() // (1) happy-path defaults, (2) all deps inside but without example
      val created = createdProductResponse()
      val toUpdate = updateProductRequest()
      val updated = updatedProductRequest()

      // when
      val id = postProduct(toCreate).let {
         // Can be read only once, therefore must be saved, then check null, etc.
         // So much boilerplate code... will fix in the next `5_WriteClient`.
         val body = it.body
         val id = body!!.id

         // then
         it.status shouldBeEqualTo OK
         body.withIgnoredId() shouldBeEqualTo created
         id
      }

      // when
      patchProduct(id, toUpdate).let {

         // then
         it.status shouldBeEqualTo OK
         it.body shouldBeEqualTo updated.copy(id = id)
      }
   }

   // This is the test above but refactored with the 2nd approach to chained factories.
   //
   // Additionally used techniques:
   // (3) Mapping factories
   // (4) Random
   @Test
   fun `should update product (WITH CHAINED FACTORIES)`() {
      // given
      val toCreate = createProductRequest()
      val created = toCreate.toProductResponse().withNextVersion() // (3) mapping
      val toUpdate = created.withRandomUpdate() // (4) random
      val updated = toUpdate.toProductRespons().withNextVersion()

      // when
      val id = postProduct(toCreate).let {
         // Can be read only once, therefore must be saved, then check null, etc.
         // So much boilerplate code... will fix in the next `5_WriteClient`.
         val body = it.body
         val id = body!!.id

         // then
         it.status shouldBeEqualTo OK
         body.withIgnoredId() shouldBeEqualTo created
         id
      }

      // when
      patchProduct(id, toUpdate).let {

         // then
         it.status shouldBeEqualTo OK
         it.body shouldBeEqualTo updated.copy(id = id)
      }
   }
}

interface CoupledProductBuilder {

   // (1) + (2) Always provide all access to all dependencies and happy-path defaults!
   // This will make your methods easy to use and reusable.
   fun createProductRequest(
      name: String = "name",
      description: String = "description",
      price: BigDecimal = BigDecimal("1.00"),
   ): CreateProductRequest =
      CreateProductRequest(
         name = name,
         description = description,
         price = price
      )

   // This is better than repeating it in every test, BUT it's still mid!
   // You need to keep track of the data set in the previous method, which is PAIN!
   fun createdProductResponse(
      id: UUID = IGNORED_ID, // Ignore this ugly for now, we'll fix it in the next `5_WriteClient`
      name: String = "name",
      description: String = "description",
      price: BigDecimal = BigDecimal("1.00"),
      version: Long = 1L,
   ): ProductResponse =
      ProductResponse(
         id = id,
         name = name,
         description = description,
         price = price,
         version = version,
      )

   // Again, I need to know what was previously set in "create" method, to not set
   // fields to exactly the same value.
   fun updateProductRequest(
      name: Optional<String>? = Optional.of("new name"),
      description: Optional<String>? = Optional.of("new description"),
      price: Optional<BigDecimal>? = Optional.of(BigDecimal("100.00")),
      version: Long = 1L, // And it represents only 1st update! Cannot use for 2nd, 3rd and so on.
   ): UpdateProductRequest =
      UpdateProductRequest(
         name = name,
         description = description,
         price = price,
         version = version,
      )

   fun updatedProductRequest(
      id: UUID = IGNORED_ID,
      name: String = "new name",
      description: String = "new description",
      price: BigDecimal = BigDecimal("100.00"),
      version: Long = 2L,
   ): ProductResponse =
      ProductResponse(
         id = id,
         name = name,
         description = description,
         price = price,
         version = version,
      )
}

interface ChainedProductBuilder {

   // You can perfectly predict the values of response :)
   // Also less code than the previous approach.
   fun CreateProductRequest.toProductResponse(): ProductResponse =
      ProductResponse(
         id = IGNORED_ID, // Ignore this ugly for now, we'll fix it in the next `5_WriteClient`
         name = name,
         description = description,
         price = price,
         version = 0L,
      )

   fun ProductResponse.withNextVersion() =
      copy(version = version + 1L)

   // (3) Random change always makes sense :) You decide what part is random.
   fun ProductResponse.withRandomUpdate(): UpdateProductRequest =
      UpdateProductRequest(
         name = Optional.ofNullable(randomIntsString()),
         description = Optional.ofNullable(randomIntsString()),
         price = Optional.ofNullable(BigDecimal("${randomIntsString()}.00")),
         version = version,
      )

   // (2) Version without random but with all deps configurable if needed
   fun ProductResponse.withUpdated(
      name: Optional<String>? = Optional.ofNullable(this.name),
      description: Optional<String>? = Optional.ofNullable(this.description),
      price: Optional<BigDecimal>? = Optional.ofNullable(this.price),
      version: Long = this.version,
   ): UpdateProductRequest =
      UpdateProductRequest(
         name = name,
         description = description,
         price = price,
         version = version,
      )

   fun UpdateProductRequest.toProductRespons(): ProductResponse =
      ProductResponse(
         id = IGNORED_ID,
         name = name?.getOrNull(), // NOT THE BEST!! Cannot be used for testing not modified attributes!! We'll fix in `5_WriteClient`
         description = description?.getOrNull(),
         price = price?.getOrNull(),
         version = version,
      )
}

fun randomIntsString(): String = Random.nextInt(from = 1111, until = 9999).toString()