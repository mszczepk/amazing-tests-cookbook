package mszczepk.atc.recipes

import mszczepk.atc.IntegrationTest
import mszczepk.atc.ProductController.Companion.PRODUCTS_URI
import mszczepk.atc.ProductController.Companion.PRODUCT_URI
import mszczepk.atc.ProductController.CreateProductRequest
import mszczepk.atc.ProductController.ProductListResponse
import mszczepk.atc.ProductController.ProductResponse
import mszczepk.atc.ProductController.UpdateProductRequest
import org.amshove.kluent.shouldBeEqualTo
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.HttpStatus.OK
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.test.web.reactive.server.FluxExchangeResult
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.returnResult
import java.math.BigDecimal
import java.util.UUID
import kotlin.test.BeforeTest
import kotlin.test.Test

class `3_DRY` : IntegrationTest(), ProductApi {

   @BeforeTest
   fun reset() {
      deleteProducts()
   }

   // This is the previous example from `2_UseApi` but with ProductApi trait extracted.
   // Cons:
   // + Much shorter test
   // + Reusable piece of code
   // + Lower entry barrier to start writing tests
   // + Centralized API documentation
   // + You may not need to change tests if your API does not change major version! Only mapping
   @Test
   fun `should list products for checkout (DRY)`() {
      // given
      val toCreate = CreateProductRequest(
         name = "name",
         description = null,
         price = BigDecimal("1.00"),
      )
      val expected = ProductResponse(
         id = IGNORED_ID,
         name = "name",
         description = null,
         price = BigDecimal("1.00"),
         version = 1L,
      )

      // and
      postProduct(toCreate).status shouldBeEqualTo OK

      // when
      val response = getProducts()

      // then
      response.status shouldBeEqualTo OK
      response.body?.products?.single()?.withIgnoredId() shouldBeEqualTo expected
   }
}

interface ProductApi {
   val client: WebTestClient

   fun postProduct(request: CreateProductRequest): FluxExchangeResult<ProductResponse> =
      client.post()
         .uri(PRODUCTS_URI)
         .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
         .bodyValue(request)
         .exchange()
         .returnResult(ProductResponse::class.java)

   fun getProducts(): FluxExchangeResult<ProductListResponse> =
      client.get()
         .uri(PRODUCTS_URI)
         .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
         .exchange()
         .returnResult(ProductListResponse::class.java)

   fun deleteProducts(): FluxExchangeResult<Any> =
      client.delete()
         .uri(PRODUCTS_URI)
         .exchange()
         .returnResult()

   fun patchProduct(id: UUID, request: UpdateProductRequest): FluxExchangeResult<ProductResponse> =
      client.patch()
         .uri(PRODUCT_URI, id)
         .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
         .bodyValue(request)
         .exchange()
         .returnResult(ProductResponse::class.java)
}