package mszczepk.atc.recipes

import mszczepk.atc.IntegrationTest
import mszczepk.atc.ProductController.Companion.PRODUCTS_URI
import mszczepk.atc.ProductController.CreateProductRequest
import mszczepk.atc.ProductController.ProductListResponse
import mszczepk.atc.ProductController.ProductResponse
import mszczepk.atc.ProductDao
import org.amshove.kluent.shouldBeEqualTo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import java.math.BigDecimal
import java.util.UUID.randomUUID
import kotlin.test.BeforeTest
import kotlin.test.Test
import mszczepk.atc.ProductController.ProductResponse as ProductEntity

class `2_UseApi` : IntegrationTest() {

   @Autowired
   lateinit var productDao: ProductDao // Test is coupled with implementation detail!!

   @BeforeTest
   fun reset() {
      productDao.clear()
   }

   // Shows dangers of mixing API detail levels.
   // In-memory store used for simplicity, but applicable for real scenarios as well.
   @Test
   fun `should list products for checkout`() {
      // given
      val p1 = ProductEntity( // Dependency to yet another class!! The number of dependencies will increase for sure!
         id = randomUUID(),
         name = "name",
         description = null,
         price = BigDecimal("-1.00"), // BUG in test! Negative price? What if this feature is a critical path for the checkout domain?!
//         price = BigDecimal("1.00"), // uncomment after refactor
         version = 1L,
      )
      val p2 = ProductEntity(
         id = randomUUID(),
         name = "", // Apparently, this is invalid as well if you go see the main src.
         description = null,
         price = BigDecimal("100.00"),
//         price = BigDecimal("100.00"), // uncomment after refactor
         version = 0L, // Well... this also never happens! Reader can come to wrong conclusions about the system!!
      )
      productDao[p1.id] = p1 // Easy and no problem! :(
      productDao[p2.id] = p2

      // when
      val response = client.get()
         .uri(PRODUCTS_URI)
         .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
         .exchange()
         .returnResult(ProductListResponse::class.java)
         .body!!
         .products
         .toSet()

      // then
      response shouldBeEqualTo setOf(p1, p2)
      // Test passes, but incorrect assumptions were made! Important domain logic was completely skipped!
      // This test fails to provide sense of security, correctness and knowledge.
   }

   // Try to refactor ProductDao to ProductRepository!
   // All test needs to change as well even though the functionality remains unchanged...
   // SRP broken for tests :/


   // The same as above with used API.
   // Shows that the test doesn't pass with invalid setup and that you don't need to change
   // ANY test in the classes that use API approach.
   @Test
   fun `should list products for checkout (USED API)`() {
      // given
      val toCreate = CreateProductRequest( // Same abstraction level. No need to change after refactor :)
         name = "name",
         description = null,
         price = BigDecimal("-1.00"), // Same BUG as above!
//         price = BigDecimal("1.00"), // uncomment to check if works
      )
      val expected = ProductResponse(
         id = IGNORED_ID,
         name = "name",
         description = null,
         price = BigDecimal("-1.00"),
//         price = BigDecimal("1.00"), // uncomment to check if works
         version = 1L,
      )

      // and
      client.post()
         .uri(PRODUCTS_URI)
         .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
         .bodyValue(toCreate)
         .exchange()
         .expectStatus()
         .isOk
      // If the setup is still invalid, this fails (note it's still a "given" section!)
      // It lets you know you made incorrect assumptions as soon as possible :)

      // when
      val response = client.get()
         .uri(PRODUCTS_URI)
         .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
         .exchange()
         .returnResult(ProductListResponse::class.java)
         .body!!
         .products
         .singleOrNull()
         ?.withIgnoredId()

      // then
      response shouldBeEqualTo expected
      // Likely IRL use case was tested (and contract as well).
   }
}