Amazing Tests Cookbook
===

Requirements:
* JDK 17+
* InteliJ IDEA
* Markdown Plugin enabled

Presentation slides can be found [here](slides-pl.md),
BUT I don't recommend reading them without IDE :)
